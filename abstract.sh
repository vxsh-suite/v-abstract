#!/bin/bash
# generates tidy commit messages based on VERSION
# I wanted to just put this functionality in VRaptor but it's not ready so I'll just use bash
alpha="alpha"
beta="beta"
rc="rc"
# as long as there are arguments, assume final argument is field to write
if [[ -n "$1" ]]; then
 field=${!#}
fi

check_if_repo () {
 # attempt to do "git status" and continue if it's possible
 git status 2&> /dev/null && echo "repo"
}

lengthen () { # zerofill number to two places
 if (( $1 < 10 )); then
   echo "0$1"
  else
   echo "$1"
 fi
}

cat_if_exists () { # print file ($1) or default value ($2)
 if [[ -f $1 ]]; then
   cat $1
  else
   echo $2
 fi
}

commit_message () { # assemble commit message from VERSION
 # LATER: get top level of git repo so you don't have to run this from the top
 _d=./VERSION
 
 abstract=`cat $_d/abstract`
 answering=`cat $_d/answering`
 codename=`cat_if_exists $_d/codename ""`
 epoch=`cat_if_exists $_d/epoch "0"`
 major=`cat $_d/major`
 minor=`cat $_d/minor`
 point=`cat $_d/point`
 rabc=`cat_if_exists $_d/rabc "false"`
 rabcv=`cat_if_exists $_d/rabcv "1"`
 raur=`cat_if_exists $_d/raur "1"`
 regen=`cat_if_exists $_d/regen "0"`
 repair=`cat_if_exists $_d/repair "0"`
 ry=`cat_if_exists $_d/ry "0"`
 rym=`cat_if_exists $_d/rym "0"`
 rymd=`cat_if_exists $_d/rymd "0"`
 source=`cat_if_exists $_d/source "0"`
 stamped=`cat_if_exists $_d/stamped "true"`

 _timestamp=`date +%s --date "${ry}-${rym}-${rymd} 00:01"`
 _longdate=`date +"%Y.%m.%d" --date "@${_timestamp}"`

 _majorlong=`lengthen $major`
 _minorlong=`lengthen $minor`
 _pointlong=`lengthen $point`
 _regenlong=`lengthen $regen`
 _repairlong=`lengthen $repair`
 _sourcelong=`lengthen $source`
 _corelength=3
 
 # do a sanity check on prerelease/"rabc"
 if [[ "$rabc" == "false" || -z "$rabc" ]];
  # if the prerelease is totally empty, remove the number
  then
   rabcv=0
  # otherwise, normalise prerelease
  else
   case $rabc in
    "alpha")
       rabc="$alpha" ;;
    "beta")
       rabc="$beta" ;;
    "a")
       rabc="$alpha" ;;
    "b")
       rabc="$beta" ;;
    "rc")
       rabc="$rc" ;;
   esac
 fi
 
 _core="${major}.${minor}"
 # add epoch if > 0
 if (( $epoch > 0 )); then
   _core="${epoch}:${_core}"
 fi
 _build=${_longdate}.${_sourcelong}
 
 # add prerelease tags
 # if given an alpha/beta/rc number, put all prerelease tags in build string
 if (( $rabcv > 0 )); then
   _core="${_core}.${point}-${rabc}.${rabcv}"
  # if given build/"regen", put "regen" & "repair" in build string
  elif (( $regen > 0 )); then
   _core="${_core}.${point}"
  # if only given "repair", put "repair" in core string
  elif (( $repair > 0 )); then
   _core="${_core}.${point}.${_repairlong}"
   _corelength=4
  # if given no prerelease tags, assume there will be a lot of point versions
  else
   _core="${_core}.${_pointlong}"
   _corelength=4  # not actually true but it omits "regen"/"repair"
 fi
 
 if (( $_corelength < 4 )); then
   _build="${_regenlong}.${_repairlong}.${_build}"
 fi
 # add distribution package release if > 1
 if (( $raur > 1 )); then
   _core="${_core}-${raur}"
 fi
 long_version="ver ${_core}+${_build}"
 
 # add codename if present
 if [[ -n "$codename" ]]; then
   long_version="$long_version ($codename)"
 fi
 
 # add CalVer only if abstract is not too long
 # it's recommended that git commit headings are < 50 characters
 _titlestr="$abstract"
 _textlength=`echo $abstract | wc --chars`
 
 # mark "unstamped" updates as temporary
 if [[ ! "$stamped" == "true" ]]; then
   if (( $_textlength < 32 )); then
    _titlestr=`date +"$_titlestr; update %y-%-m-%d+${source}" --date "@${_timestamp}"`
   fi
   _titlestr="$_titlestr [TEMP]"
  else
   _titlestr="$_titlestr"
 fi
 
 # put together title and body of commit
 # if body is empty, omit it
 _textlength=`echo $answering | wc --chars`
 _textlength=`expr ${_textlength} \> 1 1>&/dev/null && echo $answering | wc --lines`
 # BUG: this comparison fires an error if answering is actually empty
 if (( $_textlength > 0 )); then
   _message=`echo -e "${_titlestr}\n\n$answering\n\n${long_version}"`
  else
   _message=`echo -e "${_titlestr}\n\n${long_version}"`
 fi
 
 # output commit message
 echo "$_message" | tee ${_d}/COMMIT_EDITMSG
}

write_field () {
 # LATER: get top level of git repo so you don't have to run this from the top
 _d=./VERSION
 value=$1
 nano ${_d}/${value} && commit_message && git add ${_d} && git commit --amend -F ${_d}/COMMIT_EDITMSG
}


# if this is "probably" a git repo, then proceed
in_repo=`check_if_repo`
if [[ -n "$in_repo" ]]; then
  if [[ -n "$field" ]]; then
    write_field $field
   else
    commit_message
  fi
 else
  echo "This does not seem to be a git repository." 1>&2
  exit 1
fi
